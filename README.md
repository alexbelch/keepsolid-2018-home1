# Home 1#

Самостоятельно развернуть:
1. Nginx
2. PHP 7.1 (PHP-FPM)
3. Тестовое приложение на виртуальном хосте test.com

Написать простую программу "Hi! This is test.com" и запустить в браузере по test.com.

Результат работы: конфигурация test.com, а так же файл с приложением Hi! This is test.com.

В этот же архив добавить файл README.md, в котором описать, как поднимать окружение и пирложение.

#Как поднимать окружение.#
1. Поднять виртуальную машину с Ubuntu 16.04
	- Скачиваем VirtualBox: https://www.virtualbox.org/wiki/Downloads и устанавливаем.
	- Скачиваем образ Ubuntu 16.04 для 64-битных систем: http://old-releases.ubuntu.com/releases/16.04.0/ubuntu-16.04-desktop-amd64.iso и устанавливаем образ в виртуалку.

2. Устанавливаем PHP 7.1 на Ubuntu 
	- переходим по ссылке https://www.rosehosting.com/blog/install-php-7-1-with-nginx-on-an-ubuntu-16-04-vps/ и выполняем действия согласно инструкции

3. Устанавливаем Nginx на Ubuntu 
    - переходим по ссылке https://www.rosehosting.com/blog/install-php-7-1-with-nginx-on-an-ubuntu-16-04-vps/ и выполняем действия согласно инструкции.

4. Устанавливаем Git на Ubuntu
	- переходим по ссылке https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-16-04 и выполняем действия согласно инструкции.

5. Cоздаем репозиторий на https://bitbucket.org и клонируем его к себе.

6. Заходим в свой локальный репозиторий и в нем создаем файлы конфигураций и программу, также правим README.md.

7. Link for download result screenshot https://bitbucket.org/alexbelch/keepsolid-2018-home1/downloads/Screenshot_from_2018-07-03%2018-22-52.png