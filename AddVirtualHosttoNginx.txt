add new virtual host to nginx

1.create dir with name test.com and file index.php (/home/alex/AlexB/test.com)

2.copy exist file default or example.com and rename to test.com
$ sudo cp  /etc/nginx/sites-available/example.com /etc/nginx/sites-available/test.com
[sudo] password for alex:

3.edit file /etc/nginx/sites-available/test.com change server_name and root
$sudo nano /etc/nginx/sites-available/test.com

4.create symlink
$sudo ln -s /etc/nginx/sites-available/test.com /etc/nginx/sites-enabled/test.com

5.add this host to file /etc/hosts
$sudo nano /etc/hosts

6.restart nginx
$sudo service nginx restart

7.test in browser http://test.com


8.see status service nginx
$ systemctl status nginx.service
● nginx.service - A high performance web server and a reverse proxy server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: active (running) since вт 2018-07-03 10:32:18 EEST; 11min ago
  Process: 2730 ExecStop=/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /run/nginx.pid (code=exited, stat
  Process: 2735 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
  Process: 2731 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
 Main PID: 2739 (nginx)
   CGroup: /system.slice/nginx.service
           ├─2739 nginx: master process /usr/sbin/nginx -g daemon on; master_process on
           └─2740 nginx: worker process

лип 03 10:32:18 alex-VirtualBox systemd[1]: Stopped A high performance web server and a reverse proxy server.
лип 03 10:32:18 alex-VirtualBox systemd[1]: Starting A high performance web server and a reverse proxy server...
лип 03 10:32:18 alex-VirtualBox systemd[1]: nginx.service: Failed to read PID from file /run/nginx.pid: Invalid argument
лип 03 10:32:18 alex-VirtualBox systemd[1]: Started A high performance web server and a reverse proxy server.

9.see error log nginx
$ tail /var/log/nginx/error.log

10.see process php
$ ps aux | grep php
root      2686  0.0  0.9 308400 36840 ?        Ss   10:25   0:00 php-fpm: master process (/etc/php/7.1/fpm/php-fpm.conf)
www-data  2687  0.0  0.1 308400  6648 ?        S    10:25   0:00 php-fpm: pool www
www-data  2688  0.0  0.1 308400  6648 ?        S    10:25   0:00 php-fpm: pool www
alex      2701  0.0  0.0  22796   876 pts/17   S+   10:29   0:00 grep --color=auto php

11.see status service php7.1-fpm
$ systemctl status php7.1-fpm.service
● php7.1-fpm.service - The PHP 7.1 FastCGI Process Manager
   Loaded: loaded (/lib/systemd/system/php7.1-fpm.service; enabled; vendor preset: enabled)
   Active: active (running) since вт 2018-07-03 10:26:00 EEST; 17s ago
     Docs: man:php-fpm7.1(8)
 Main PID: 2686 (php-fpm7.1)
   Status: "Processes active: 0, idle: 2, Requests: 0, slow: 0, Traffic: 0req/sec"
   CGroup: /system.slice/php7.1-fpm.service
           ├─2686 php-fpm: master process (/etc/php/7.1/fpm/php-fpm.conf)
           ├─2687 php-fpm: pool www
           └─2688 php-fpm: pool www

12.see who listen port 80
$ sudo netstat -tulpn | grep :80
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      830/nginx -g daemon
tcp6       0      0 :::80                   :::*                    LISTEN      830/nginx -g daemon

13.see process nginx
$ps aux | grep nginx
root       830  0.0  0.0 123320  1480 ?        Ss   09:49   0:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data   831  0.0  0.0 123664  3184 ?        S    09:49   0:00 nginx: worker process
alex      2330  0.0  0.0  22796   948 pts/17   S+   09:57   0:00 grep --color=auto nginx

14.see all connection
$sudo netstat -tulpn
[sudo] password for alex:
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      830/nginx -g daemon
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      625/cupsd
tcp6       0      0 :::80                   :::*                    LISTEN      830/nginx -g daemon
tcp6       0      0 ::1:631                 :::*                    LISTEN      625/cupsd
udp        0      0 0.0.0.0:68              0.0.0.0:*                           889/dhclient
udp        0      0 0.0.0.0:631             0.0.0.0:*                           713/cups-browsed
udp        0      0 0.0.0.0:56398           0.0.0.0:*                           637/avahi-daemon: r
udp    21504      0 0.0.0.0:5353            0.0.0.0:*                           637/avahi-daemon: r
udp6       0      0 :::37585                :::*                                637/avahi-daemon: r
udp6   15360      0 :::5353                 :::*                                637/avahi-daemon: r
